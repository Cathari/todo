@extends('layouts.app')

@section('title', 'Create task')

@section('content')
<div class="row">
    <div class="col-lg-6 mx-auto">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="POST" action="{{ route('tasks.store') }}">
            @csrf
                <div class="form-group">
                    <label for="task-title">Task name</label>
                    <input type="text" name="title"
                           value="{{ old('title') }}" class="form-control" id="exampleFormControlInput1">
                </div>

                <div class="form-group">
                    <label for="task-description">Task description</label>
                    <textarea class="form-control" name="description" id="task-description" rows="3">{{ old('description') }}</textarea>
                </div>
                <button type="submit" class="btn btn-success">Create</button>
        </form>
    </div>
</div>


@endsection
