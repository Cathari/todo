@extends('layouts.app')

@section('title', 'All tasks')

@section('content')

<a href="{{ route('tasks.create') }}" class="btn btn-success">Create task</a>

@if(session()->get('success'))
    <div class="alert alert-success mt-3">
        {{ session()->get('success') }}
    </div>
@endif


<div class="d-flex justify-content-start rounded-lg mt-4 mb-4 bg-dark text-light">
    <div class="col-sm-1 p-2 text-center">ID</div>
    <div class=" col-sm-2 p-2">Task name</div>
    <div class="p-2">Task description</div>
</div>


<p id="demo">Click me.</p>
<p>aloha</p>

<test-component></test-component>





@foreach($allTasks as $tasks)

    <div class="task d-flex justify-content-start bg-warning rounded-lg draggable mt-2 mb-2" draggable="true">

        <h5 class="col-sm-1 p-2 text-center align-self-center">{{$tasks->id}}.</h5>
        <div class="col-sm-2 p-2 align-self-center">{{$tasks->title}}</div>
        <div class="col-sm p-2 align-self-center">{{$tasks->description}}</div>

        <div class="d-inline-flex align-self-center">
            <div class="btn">
                <a href="{{ route('tasks.show', $tasks) }}" class="btn btn-success">
                    <i class="fa fa-eye"></i>
                </a>
            </div>
            <div class="btn">
                <a href="{{ route('tasks.edit', $tasks) }}" class="btn btn-primary">
                    <i class="fa fa-pencil"></i>
                </a>
            </div>
            <div class="btn">
                <form method="POST" action="{{ route('tasks.destroy', $tasks) }}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">
                        <i class="fa fa-trash-o"></i>
                    </button>
                </form>
            </div>
        </div>

                        {{-- HEY BRO WHATS UP WITH YOUR CODE --}}
        <div class="custom-control custom-checkbox align-self-center mr-2 ml-2">
            <form method="POST" action="{{ route('tasks.store', $tasks) }}">
                @csrf
                <input type="checkbox" class="custom-control-input" onclick="chbTest()"
                       @if ($tasks->checked === 1)
                       checked="checked"
                       @endif
                       id="{{$tasks->id}}">
                <label class="custom-control-label" for={{$tasks->id}}></label>
            </form>
        </div>

    </div>

@endforeach

@endsection
