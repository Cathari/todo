@extends('layouts.app')

@section('title', 'View task '.$tasks->id)

@section('content')

<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">{{ $tasks->title }}</h4>
            <p class="card-text">{{ $tasks->description }}</p>
        </div>
    </div>
</div>

@endsection
