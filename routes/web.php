<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('layouts.app');
});

Route::middleware('auth')->group(function() {
    Route::resource('tasks', 'TasksController');
    Route::get('/home', 'HomeController@index')->name('home');
});

Auth::routes();




